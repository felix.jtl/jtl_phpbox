<?php declare(strict_types=1);

namespace Plugin\jtl_phpbox;

use JTL\Events\Dispatcher;
use JTL\Plugin\Bootstrapper;

/**
 * Class Bootstrap
 * @package Plugin\jtl_phpbox
 */
class Bootstrap extends Bootstrapper
{
    public function boot(Dispatcher $dispatcher): void
    {
        parent::boot($dispatcher);
        $dispatcher->listen('shop.hook.' . \HOOK_SMARTY_INC, function (array $args) {
            $args['smarty']->assign('jtl_phpbox_number', 1234)
                ->assign('jtl_phpbox_string', 'Hello World!');
        });
    }
}
